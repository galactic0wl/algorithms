function reverseForOf(string) {
    let result = '';
    for (const char of string) {
        result = char + result;
    }
    return result;
}
function reverseForLoop(string) {
    let result = '';
    for (let i = 0; i < string.length; i++) {
        result = string[i] + result;
    }
    return result;
}
function reverseForEach(string) {
    let result = '';
    [...string].forEach(c => {
        result = c + result;
    });
    return result;
}
function reverseArray(string) {
    let result = [...string].reverse().join('');
    return result;
}
function reverseAccumulator(string) {
    let result = [...string].reduce((prev, next) => {
        return next + prev;
    });
    return result;
}
function reverseRecursive(input) {
    if(input === '' || input.length === 1) {
        return input
    }
    const chars = [...input];
    const nextChar = chars.shift();
    return reverseRecursive(chars.join('')) + nextChar;
}

module.exports = { reverseForOf, reverseForLoop, reverseForEach, reverseArray, reverseAccumulator, reverseRecursive }