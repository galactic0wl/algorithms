function isPalindromReversal(string) {
    let result = [...string].reverse().join('');
    return result === string;
}

function isPalindromeCharWalk(string){
    let result = true;
    for(let i = 0; i < string.length; i++) {
        result = string[i] === string[string.length - 1 - i];
    }
    return result;
}

module.exports = { isPalindromReversal, isPalindromeCharWalk }