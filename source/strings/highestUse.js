function highestUsedCharMap(string) {

    if(!string || string.length === 1) {
        return string;
    }

    let result = [null,0];
    const charMap = new Map();
    for(const char of string) {
        if(char === '' || char === ' '){
            continue;
        }
        charMap.set(char.toLowerCase(), charMap.get(char.toLowerCase()) + 1 || 1);
    }

    charMap.forEach((value, key) => {
        if(value > 1 && value > result[1]) {
            result[0] = key;
            result[1] = value;
        }
    });

    return result[0];
}

function highestUsedCharObj(string) {

    if(!string || string.length === 1) {
        return string;
    }

    let result = {character: '', count: 0};
    const charObj = {};
    for(const char of string) {
        if(char === '' || char === ' '){
            continue;
        }
        charObj[char.toLowerCase()] = charObj[char.toLowerCase()] + 1 || 1;
    }
    for(const prop in charObj) {
        if(charObj[prop] > result.count) {
            result.character = prop;
            result.count = charObj[prop];
        }
    }
    return result.character;
}

module.exports = { highestUsedCharMap, highestUsedCharObj }