function reverseIntegerArray(number) {
    const sign = Math.sign(number);
    const numbers = [...number.toString()];
    if (sign === -1) { numbers.shift(); }
    numbers.reverse();
    return parseInt(numbers.join('')) * sign;
}

function reverseIntegerArrayAbs(number) {
    const sign = Math.sign(number);
    number = Math.abs(number);
    const numbers = [...number.toString()];
    return parseInt(numbers.reverse().join('')) * sign;
}

module.exports = { reverseIntegerArray, reverseIntegerArrayAbs }