function chunk(arr, chunkSize) {
    const result = [];
    
    while(arr.length > 0) {
        result.push(arr.splice(0, chunkSize));
    }
    return result;
}

module.exports = { chunk }