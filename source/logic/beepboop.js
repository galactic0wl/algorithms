// FizzBuzz is getting boring, let's go with BeepBoop instead.
function beepboop(inputNumber) {
    const result = [];

    count = 1;
    while(count <= inputNumber) {
        if(count % 3 === 0 && count % 5 === 0) {
            result.push('BeepBoop');
        } else if (count % 3 === 0) {
            result.push('Beep');
        } else if (count % 5 === 0) {
            result.push('Boop');
        } else {
            result.push(count);
        }
        count++;
    }

    return result;
}

module.exports = { beepboop }