const chai = require('chai');
const assertArrays = require('chai-arrays');
chai.use(assertArrays);
const expect = chai.expect;
const { chunk } = require('../source/arrays/chunk');

const chunkRuns = [
    { it: 'Should split array in to a jagged array where each arrays length is n, overflow remainders.', options: { inputArray: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0], chunkSize: 2, expected: [[1, 2], [3, 4], [5, 6], [7, 8], [9, 0]] } },
    { it: 'Should split array in to a jagged array where each arrays length is n, overflow remainders.', options: { inputArray: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0], chunkSize: 3, expected: [[1, 2, 3], [4, 5, 6], [7, 8, 9], [0]] } },
    { it: 'Should split array in to a jagged array where each arrays length is n, overflow remainders.', options: { inputArray: ['han', 'solo', 'is', 'a', 'hero'], chunkSize: 2, expected: [['han', 'solo'], ['is', 'a'], ['hero']] } }
];

describe('---- Arrays ----', () => {
    describe('chunk(arr, chunkSize)', () => {
        chunkRuns.forEach(run => {
            it(run.it, () => {
                // Arrange
                const { inputArray, chunkSize, expected } = run.options;
                // Act
                const actual = chunk(inputArray, chunkSize);
                // Assert
                expect(actual).to.eql(expected);
            });
        });
    });
});