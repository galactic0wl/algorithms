const expect = require('chai').expect;
const { reverseIntegerArray, reverseIntegerArrayAbs } = require('../source/integers/reverse');

describe('---- Integer Reversals ----', () => {
    describe('reverseIntegerArray(number)', () => {
        it('Should reverse the input number and return it.', () => {
            // Arrange
            const inputNumber = 118;
            const expected =  811;
            // Act
            const actual = reverseIntegerArray(inputNumber);
            // Assert
            expect(actual).to.be.equal(expected);
        });
        it('Should reverse the input number and return it.', () => {
            // Arrange
            const inputNumber = 371;
            const expected =  173;
            // Act
            const actual = reverseIntegerArray(inputNumber);
            // Assert
            expect(actual).to.be.equal(expected);
        });
        it('Should persist the sign of the input number after reversal.', () => {
            // Arrange
            const inputNumber = -100;
            const expected =  -1;
            // Act
            const actual = reverseIntegerArray(inputNumber);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
    describe('reverseIntegerArrayAbs(number)', () => {
        it('Should reverse the input number and return it.', () => {
            // Arrange
            const inputNumber = 118;
            const expected =  811;
            // Act
            const actual = reverseIntegerArrayAbs(inputNumber);
            // Assert
            expect(actual).to.be.equal(expected);
        });
        it('Should reverse the input number and return it.', () => {
            // Arrange
            const inputNumber = 371;
            const expected =  173;
            // Act
            const actual = reverseIntegerArrayAbs(inputNumber);
            // Assert
            expect(actual).to.be.equal(expected);
        });
        it('Should persist the sign of the input number after reversal.', () => {
            // Arrange
            const inputNumber = -100;
            const expected =  -1;
            // Act
            const actual = reverseIntegerArrayAbs(inputNumber);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
});