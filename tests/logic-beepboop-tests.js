const chai = require('chai');
const assertArrays = require('chai-arrays');
chai.use(assertArrays);
const expect = chai.expect;
const { beepboop } = require('../source/logic/beepboop');

describe('---- Logic BeepBoop ----', () => {
    describe('beepboop(number)', () => {
        it('Should add Beep to the array if n is divisible by 3, Boop if n is divisible by 5, BeepBoop if n is divisible by both, or n if not at all.', () => {
            // Arrange
            const inputNumber = 21;
            const expected =  [1,2,'Beep',4,'Boop','Beep',7,8,'Beep','Boop',11,'Beep',13,14,'BeepBoop',16,17,'Beep',19,'Boop','Beep'];
            // Act
            const actual = beepboop(inputNumber);
            // Assert
            expect(actual).to.be.equalTo(expected);
        });
    });
});