const expect = require('chai').expect;
const { isPalindromReversal, isPalindromeCharWalk } = require('../source/strings/palindrome');

describe('---- Palindromes ----', () => {
    describe('isPalindromReversal(string)', () => {
        it('Should return true if the input string is a palindrome', () => {
            // Arrange
            const inputString = 'AxxA';
            // Act
            const actual = isPalindromReversal(inputString);
            // Assert
            expect(actual).to.be.true;
        });
        it('Should return false if the input string is not a palindrome', () => {
            // Arrange
            const inputString = 'Snowboard';
            // Act
            const actual = isPalindromReversal(inputString);
            // Assert
            expect(actual).to.be.false;
        });
    });
    describe('isPalindromeCharWalk(string)', () => {
        it('Should return true if the input string is a palindrome', () => {
            // Arrange
            const inputString = 'AxxA';
            // Act
            const actual = isPalindromeCharWalk(inputString);
            // Assert
            expect(actual).to.be.true;
        });
        it('Should return false if the input string is not a palindrome', () => {
            // Arrange
            const inputString = 'Snowboard';
            // Act
            const actual = isPalindromeCharWalk(inputString);
            // Assert
            expect(actual).to.be.false;
        });
    });
});