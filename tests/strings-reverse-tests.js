const expect = require('chai').expect;
const { reverseForOf, reverseForLoop, reverseForEach, reverseArray, reverseAccumulator, reverseRecursive } = require('../source/strings/reverse');

describe('---- String Reversals ----', () => {
    describe('reverseForOf(string)', () => {
        it('Should reverse the input string and return the result', () => {
            // Arrange
            const inputString = 'Apple';
            const expected = 'elppA';
            // Act
            const actual = reverseForOf(inputString);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
    describe('reverseForLoop(string)', () => {
        it('Should reverse the input string and return the result', () => {
            // Arrange
            const inputString = 'Apple';
            const expected = 'elppA';
            // Act
            const actual = reverseForLoop(inputString);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
    describe('reverseForEach(string)', () => {
        it('Should reverse the input string and return the result', () => {
            // Arrange
            const inputString = 'Apple';
            const expected = 'elppA';
            // Act
            const actual = reverseForEach(inputString);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
    describe('reverseArray(string)', () => {
        it('Should reverse the input string and return the result', () => {
            // Arrange
            const inputString = 'Apple';
            const expected = 'elppA';
            // Act
            const actual = reverseArray(inputString);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
    describe('reverseAccumulator(string)', () => {
        it('Should reverse the input string and return the result', () => {
            // Arrange
            const inputString = 'Apple';
            const expected = 'elppA';
            // Act
            const actual = reverseAccumulator(inputString);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
    describe('reverseRecursive(string)', () => {
        it('Should reverse the input string and return the result', () => {
            // Arrange
            const inputString = 'Apple';
            const expected = 'elppA';
            // Act
            const actual = reverseRecursive(inputString);
            // Assert
            expect(actual).to.be.equal(expected);
        });
    });
});