const expect = require('chai').expect;
const { highestUsedCharMap, highestUsedCharObj } = require('../source/strings/highestUse');

describe('---- Highest Use ----', () => {
    describe('highestUsedCharMap(string)', () => {
        it('Should return the character of the input string that is used most often.', () => {
            // Arrange
            const inputString = 'I wish I was a little bit taller. I wish I was a baller.';
            const expected = 'i';
            // Act
            const actual = highestUsedCharMap(inputString);
            // Assert
            expect(actual).to.equal(expected);
        });
        it('Should return the character of the input string that is used most often.', () => {
            // Arrange
            const inputString = 'Supercalifragilisticexpialidocious';
            const expected = 'i';
            // Act
            const actual = highestUsedCharMap(inputString);
            // Assert
            expect(actual).to.equal(expected);
        });
        it('Should return null if all characters are unique.', () => {
            // Arrange
            const inputString = 'Hey man.';
            // Act
            const actual = highestUsedCharMap(inputString);
            // Assert
            expect(actual).to.be.null;
        });
    });
    describe('highestUsedCharObj(string)', () => {
        it('Should return the character of the input string that is used most often.', () => {
            // Arrange
            const inputString = 'I wish I was a little bit taller. I wish I was a baller.';
            const expected = 'i';
            // Act
            const actual = highestUsedCharObj(inputString);
            // Assert
            expect(actual).to.equal(expected);
        });
        it('Should return the character of the input string that is used most often.', () => {
            // Arrange
            const inputString = 'Supercalifragilisticexpialidocious';
            const expected = 'i';
            // Act
            const actual = highestUsedCharObj(inputString);
            // Assert
            expect(actual).to.equal(expected);
        });
    });
});